package commands

import (
	"os"
)

// TODO: Keep a map of variables for the environment

func Chdir(dir string) error {
	return os.Chdir(dir)
}

func ChdirHome() error {
	home := os.Getenv("HOME")
	if len(home) < 1 {
		home = os.Getenv("userprofile")
	}
	return os.Chdir(home)
}
