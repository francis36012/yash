package environment

// Environment models a shell enviroment that has variables and aliases
type Environment struct {
	variables map[string]string
	aliases   map[string]string
}

// NewEnvironment creates a new Environment type
func NewEnvironment() Environment {
	return Environment{
		variables: make(map[string]string),
		aliases:   make(map[string]string),
	}
}

func (env *Environment) setVariable(key, value string) {
	env.variables[key] = value
}

func (env *Environment) getVariable(key string) string {
	return env.variables[key]
}

func (env *Environment) setAlias(name, subst string) {
	env.aliases[name] = subst
}

func (env *Environment) getAlias(name string) string {
	return env.aliases[name]
}
